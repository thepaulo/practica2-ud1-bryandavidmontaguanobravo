package com.davidmontaguano.tiendamuebles.base;

import java.time.LocalDate;

/**
 * Clase hija par diferenciar el material del cual esta hecho el mueble
 */
public class materialFabricacion extends Mueble {
    private String material;

    public materialFabricacion() {
        super();
    }

    /**
     * clase super la cual hereda del padre para tener parametros los cuales leer
     *
     * @param nombreMueble
     * @param procedencia
     * @param medida
     * @param calidad
     * @param precio
     * @param fechaCompra
     * @param material
     */
    public materialFabricacion(String nombreMueble, String procedencia, int medida, String calidad, double precio, LocalDate fechaCompra,String material) {
        super(nombreMueble, procedencia, medida, calidad, precio, fechaCompra);
        this.material=material;
    }




    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Datos del Mueble : Material de Fabricacion "+
                "\n Nombre Mueble: "+getNombreMueble()+
                "\n Procendencia: " + getProcedencia()+
                "\n Medida: " + getMedida()+
                "\n Calidad: " + getCalidad()+
                "\n Precio: " + getPrecio()+
                "\n Material de construccion: " + getMaterial()
                ;
    }
}
