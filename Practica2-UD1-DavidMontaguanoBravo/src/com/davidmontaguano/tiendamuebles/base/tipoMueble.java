package com.davidmontaguano.tiendamuebles.base;

import java.time.LocalDate;

/**
 * Clse hija que diferencia el tipo de mueble que hemos comprado y en la estancia que se usara
 */
public class tipoMueble extends Mueble {
    private String tipo;
    public tipoMueble(String nombreMueble, String procedencia, int medida, String calidad, double precio, LocalDate fechaCompra,String tipo) {
        super(nombreMueble, procedencia, medida, calidad, precio, fechaCompra);
        this.tipo=tipo;
    }

    public tipoMueble() {

    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Datos del Mueble : Tipo de Mueble "+
                "\n Nombre Mueble: "+getNombreMueble()+
                "\n Procendencia: " + getProcedencia()+
                "\n Medida: " + getMedida()+
                "\n Calidad: " + getCalidad()+
                "\n Precio: " + getPrecio()+
                "\n Tipo de Mueble: " + getTipo()
                ;
    }
}
