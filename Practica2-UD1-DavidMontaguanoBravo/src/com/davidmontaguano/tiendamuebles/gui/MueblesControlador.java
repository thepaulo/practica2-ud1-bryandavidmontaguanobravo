package com.davidmontaguano.tiendamuebles.gui;

import com.davidmontaguano.tiendamuebles.base.Mueble;
import com.davidmontaguano.tiendamuebles.base.materialFabricacion;
import com.davidmontaguano.tiendamuebles.base.tipoMueble;
import com.davidmontaguano.tiendamuebles.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * VArios metodos encargados de controllar errores y permitir la funcionalida ddel proyecto
 */
public class MueblesControlador implements ActionListener, ListSelectionListener,WindowListener{

    private Ventana vista;
    private MueblesModelo modelo;
    private File ultimaRutaExportada;

    public MueblesControlador(Ventana vista, MueblesModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    /**
     * En este metodo controlamos si es nuevo mueble o ya hay algun existente
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Nombre-Mueble\nProcedencia\nCalidad\nFecha" +
                            vista.tipoMaterialLbl.getText());
                    break;
                }

                if (modelo.existeNombre(vista.nombreTxt.getText())) {
                    Util.mensajeError("Ya existe un Mueble con esa nombre\n+" +
                            vista.nombreTxt.getText());
                    break;
                }
                if (vista.tipoMueble.isSelected()) {
                    modelo.altaTipoMueble(vista.nombreTxt.getText(),vista.procedenciaTxt.getText(),Integer.parseInt(vista.medidaTxt.getText()),vista.calidadTxt.getText()
                    ,Double.parseDouble(vista.precioTxt.getText()),vista.fechaCompraDPicker.getDate(),vista.tipoMaterialLbl.getText());
                } else {
                    modelo.altaMaterial(vista.nombreTxt.getText(),vista.procedenciaTxt.getText(),Integer.parseInt(vista.medidaTxt.getText()),vista.calidadTxt.getText()
                            ,Double.parseDouble(vista.precioTxt.getText()),vista.fechaCompraDPicker.getDate(),vista.tipoMaterialLbl.getText());
                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "materialFabricacion":
                vista.tipoMaterialLbl.setText("Material Fabricacion");
                break;
            case "tipoMueble":
                vista.tipoMaterialLbl.setText("Tipo de Mueble");
                break;

        }
    }



    private void limpiarCampos() {
        vista.nombreTxt.requestFocus();
        vista.procedenciaTxt.setText(null);
        vista.medidaTxt.setText(null);
        vista.calidadTxt.setText(null);
        vista.precioTxt.setText(null);
        vista.fechaCompraDPicker.setText(null);
        vista.tipoMaterialtxt.setText(null);
    }

    private boolean hayCamposVacios() {
        if (vista.tipoMaterialtxt.getText().isEmpty() ||
                vista.nombreTxt.getText().isEmpty()||
                vista.procedenciaTxt.getText().isEmpty()||
                vista.medidaTxt.getText().isEmpty()||
                vista.calidadTxt.getText().isEmpty()||
                vista.precioTxt.getText().isEmpty()||
                vista.fechaCompraDPicker.getText().isEmpty()){
            return true;
        }
        return false;
    }

    private void refrescar() {
        vista.dlmMueble.clear();
        for(Mueble unMueble: modelo.obtenerMuebles()){
            vista.dlmMueble.addElement(unMueble);
        }
    }
    private void addActionListener(ActionListener listener){
        vista.importarBtn.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
        vista.tipoMueble.addActionListener(listener);
        vista.Material.addActionListener(listener);
    }


    private void addWindowListener(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    private void cargarDatosConfiguracion() throws IOException{
        Properties configuracion= new Properties();
        configuracion.load(new FileReader("muebles.conf"));
        ultimaRutaExportada=new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    private void actualizarDatosConfiguracion(File ultimaRutaExportada){
        this.ultimaRutaExportada=ultimaRutaExportada;
    }

    private void guardarConfiguracion()throws IOException{
        Properties configuracion= new Properties();
        configuracion.setProperty("utlimaRutaEmportada",ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("muebles.conf"),"Datos configuracion Muebles");
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

        if(e.getValueIsAdjusting()){
            Mueble muebleSeleccionado=(Mueble)vista.list1.getSelectedValue();
            vista.nombreTxt.setText((muebleSeleccionado.getNombreMueble()));
            vista.procedenciaTxt.setText((muebleSeleccionado.getProcedencia()));
            vista.medidaTxt.setText(String.valueOf((muebleSeleccionado.getMedida())));
            vista.calidadTxt.setText((muebleSeleccionado.getCalidad()));
            vista.precioTxt.setText(String.valueOf((muebleSeleccionado.getPrecio())));
            vista.fechaCompraDPicker.setDate(muebleSeleccionado.getFechaCompra());

            if(muebleSeleccionado instanceof materialFabricacion){
                vista.Material.doClick();
                vista.tipoMaterialtxt.setText(((materialFabricacion)muebleSeleccionado).getMaterial());
            }
            else{
                vista.tipoMueble.doClick();
                vista.tipoMaterialtxt.setText(((tipoMueble)muebleSeleccionado).getTipo());
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
