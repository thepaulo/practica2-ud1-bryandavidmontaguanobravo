package com.davidmontaguano.tiendamuebles.gui;

import com.davidmontaguano.tiendamuebles.base.Mueble;
import com.davidmontaguano.tiendamuebles.base.materialFabricacion;
import com.davidmontaguano.tiendamuebles.base.tipoMueble;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase que se encarga del almacenamiento del los datos introducidos de los diferentes muebles
 */
public class MueblesModelo {

    private ArrayList<Mueble> listaMuebles;

    public MueblesModelo(){
        listaMuebles= new ArrayList<Mueble>();
    }
    public ArrayList<Mueble> obtenerMuebles() {
        return listaMuebles;
    }

    public void altaMaterial(String nombreMueble, String procedencia, int medida, String calidad, double precio, LocalDate fechaCompra,String material){
        materialFabricacion materialF= new materialFabricacion(nombreMueble,procedencia,medida,calidad,precio,fechaCompra,material);
        listaMuebles.add(materialF);
    }

    public void  altaTipoMueble(String nombreMueble, String procedencia, int medida, String calidad, double precio, LocalDate fechaCompra,String tipo){
        tipoMueble tipoM = new tipoMueble(nombreMueble, procedencia, medida, calidad, precio, fechaCompra, tipo);
        listaMuebles.add(tipoM);
    }

    public boolean existeNombre(String nombreMueble){
        for(Mueble unMueble:listaMuebles){
            if(unMueble.getNombreMueble().equals(nombreMueble)){
                return true;
            }
        }
        return false;
    }


    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demas
        Element raiz = documento.createElement("Muebles");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoMueble = null, nodoDatos = null;
        Text texto = null;

        for (Mueble unMueble : listaMuebles) {


            if (unMueble instanceof materialFabricacion) {
                nodoMueble = documento.createElement("materialFabricacion");

            } else {
                nodoMueble = documento.createElement("tipoMueble");
            }
            raiz.appendChild(nodoMueble);


            nodoDatos = documento.createElement("nombre-Mueble");
            nodoMueble.appendChild(nodoDatos);

            texto=documento.createTextNode(unMueble.getNombreMueble());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("procedencia");
            nodoMueble.appendChild(nodoDatos);

            texto=documento.createTextNode(unMueble.getProcedencia());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("medida");
            nodoMueble.appendChild(nodoDatos);

            texto=documento.createTextNode(String.valueOf(((Mueble) unMueble).getMedida()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("calidad");
            nodoMueble.appendChild(nodoDatos);

            texto=documento.createTextNode(unMueble.getCalidad());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("precio");
            nodoMueble.appendChild(nodoDatos);

            texto=documento.createTextNode(String.valueOf(((Mueble) unMueble).getPrecio()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-Compra");
            nodoMueble.appendChild(nodoDatos);

            texto = documento.createTextNode(unMueble.getFechaCompra().toString());
            nodoDatos.appendChild(texto);



            if (unMueble instanceof materialFabricacion) {
                nodoDatos = documento.createElement("material");
                nodoMueble.appendChild(nodoDatos);

                texto=documento.createTextNode(((materialFabricacion) unMueble).getMaterial());
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("tipo");
                nodoMueble.appendChild(nodoDatos);

                texto=documento.createTextNode(((tipoMueble) unMueble).getTipo());
                nodoDatos.appendChild(texto);
            }

        }
        /**
         * Guardo los datos en "fichero" que es el objeto File recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaMuebles = new ArrayList<Mueble>();
        materialFabricacion materialF = null;
        tipoMueble tipoM = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoMueble = (Element) listaElementos.item(i);


            if (nodoMueble.getTagName().equals("materialFabricacion")) {
                materialF = new materialFabricacion();
                materialF.setNombreMueble(nodoMueble.getChildNodes().item(0).getTextContent());
                materialF.setProcedencia(nodoMueble.getChildNodes().item(1).getTextContent());
                materialF.setMedida(Integer.parseInt(nodoMueble.getChildNodes().item(2).getTextContent()));
                materialF.setCalidad(nodoMueble.getChildNodes().item(3).getTextContent());
                materialF.setPrecio(Integer.parseInt(nodoMueble.getChildNodes().item(4).getTextContent()));
                materialF.setFechaCompra(LocalDate.parse(nodoMueble.getChildNodes().item(3).getTextContent()));
                materialF.setMaterial(nodoMueble.getChildNodes().item(0).getTextContent());

                listaMuebles.add(materialF);
            } else {
                if (nodoMueble.getTagName().equals("tipoMueble")) {
                    tipoM = new tipoMueble();
                    tipoM.setNombreMueble(nodoMueble.getChildNodes().item(0).getTextContent());
                    tipoM.setProcedencia(nodoMueble.getChildNodes().item(1).getTextContent());
                    tipoM.setMedida(Integer.parseInt(nodoMueble.getChildNodes().item(2).getTextContent()));
                    tipoM.setCalidad(nodoMueble.getChildNodes().item(3).getTextContent());
                    tipoM.setPrecio(Integer.parseInt(nodoMueble.getChildNodes().item(4).getTextContent()));
                    tipoM.setFechaCompra(LocalDate.parse(nodoMueble.getChildNodes().item(3).getTextContent()));

                    listaMuebles.add(tipoM);
                }
            }


        }
    }
}
