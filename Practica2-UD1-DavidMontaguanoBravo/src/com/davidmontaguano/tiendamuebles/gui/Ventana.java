package com.davidmontaguano.tiendamuebles.gui;

import com.davidmontaguano.tiendamuebles.base.Mueble;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * En este metodo nos encargamos de declarar todos los componentes de nuestra ventana
 */
public class Ventana {
    private JPanel panel1;
    public JFrame frame;
    public JRadioButton tipoMueble;
    public JRadioButton Material;
    public JLabel Nombre;
    public JLabel Procedencia;
    public JLabel Medida;
    public JLabel Calidad;
    public JLabel Precio;
    public JTextField nombreTxt;
    public JTextField procedenciaTxt;
    public JTextField medidaTxt;
    public JTextField calidadTxt;
    public JTextField precioTxt;
    public JLabel FechaCompra;
    public DatePicker fechaCompraDPicker;
    public JLabel tipoMaterialLbl;
    public JList list1;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JButton salirBtn;
    public JTextField tipoMaterialtxt;

    public DefaultListModel<Mueble> dlmMueble;

    /**
     * Este metodo se encarga de crear la ventana
     */
    public Ventana(){
        frame = new JFrame("Tienda de Muebles");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    /**
     * Metodo encargado de Iniciar los componentes de nuestra ventana
     */
    private void initComponents() {
        dlmMueble=new DefaultListModel<Mueble>();
        list1.setModel(dlmMueble);
    }


}
