package com.davidmontaguano.tiendamuebles;

import com.davidmontaguano.tiendamuebles.gui.MueblesControlador;
import com.davidmontaguano.tiendamuebles.gui.MueblesModelo;
import com.davidmontaguano.tiendamuebles.gui.Ventana;

public class Principal {

    public static void main(String[] args) {
        Ventana vista= new Ventana();
        MueblesModelo modelo=new MueblesModelo();
        MueblesControlador controlador= new MueblesControlador(vista,modelo);
    }
}
