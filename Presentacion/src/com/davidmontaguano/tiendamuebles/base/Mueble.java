package com.davidmontaguano.tiendamuebles.base;

import java.time.LocalDate;

public abstract class Mueble {

    private String nombreMueble;
    private String procedencia;
    private int medida;
    private String calidad;
    private double precio;
    private LocalDate fechaCompra;


    public Mueble(String nombreMueble, String procedencia, int medida, String calidad, double precio, LocalDate fechaCompra ){
        this.nombreMueble= nombreMueble;
        this.procedencia=procedencia;
        this.medida=medida;
        this.calidad=calidad;
        this.precio=precio;
        this.fechaCompra=fechaCompra;

    }
    public Mueble() {

    }

    public String getNombreMueble() {
        return nombreMueble;
    }

    public void setNombreMueble(String nombreMueble) {
        this.nombreMueble = nombreMueble;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public int getMedida() {
        return medida;
    }

    public void setMedida(int medida) {
        this.medida = medida;
    }

    public String getCalidad() {
        return calidad;
    }

    public void setCalidad(String calidad) {
        this.calidad = calidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }



}
