package davidmontaguano.tiendamuebles.base;

import java.time.LocalDate;

public  class MueblesRusticos extends TiendaMuebles{


    private String TipoMadera;
    public MueblesRusticos(){
        super();
    }

    public MueblesRusticos(String nombreMueble, int medida, String calidad, String movilidad, LocalDate fechaCompra, String tipoMadera){
        super(nombreMueble, medida, calidad, movilidad, fechaCompra);
        this.TipoMadera=tipoMadera;
    }

    public String getTipoMadera() {
        return TipoMadera;
    }

    public void setTipoMadera(String tipoMadera) {
        TipoMadera = tipoMadera;
    }

    @Override
    public String toString() {

        return "Datos del Mueble -->"+
                "\n Nombre Mueble: "+getNombreMueble()+
                "\n Medida: " + getMedida()+
                "\n Calidad: " + getCalidad()+
                "\n Movilidad: "+ getMovilidad()+
                "\n Fecha Compra: "+ getFechaCompra()+
                "\n Madera: " + getTipoMadera()

                ;
    }
}
