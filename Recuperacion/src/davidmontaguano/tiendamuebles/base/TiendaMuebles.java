package davidmontaguano.tiendamuebles.base;

import java.time.LocalDate;

public abstract class TiendaMuebles {

    private String nombreMueble;
    private int medida;
    private String calidad;
    private String movilidad;
    private LocalDate fechaCompra;

    public TiendaMuebles(String nombreMueble, int medida, String calidad, String movilidad, LocalDate fechaCompra) {

        this.nombreMueble=nombreMueble;
        this.medida=medida;
        this.calidad=calidad;
        this.movilidad=movilidad;
        this.fechaCompra=fechaCompra;
    }

    public TiendaMuebles(){

    }

    public String getNombreMueble() {
        return nombreMueble;
    }

    public void setNombreMueble(String nombreMueble) {
        this.nombreMueble = nombreMueble;
    }

    public int getMedida() {
        return medida;
    }

    public void setMedida(int medida) {
        this.medida = medida;
    }

    public String getCalidad() {
        return calidad;
    }

    public void setCalidad(String calidad) {
        this.calidad = calidad;
    }

    public String getMovilidad() {
        return movilidad;
    }

    public void setMovilidad(String movilidad) {
        this.movilidad = movilidad;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
}
