package davidmontaguano.tiendamuebles.base;

import java.time.LocalDate;

public class MueblesSalon extends TiendaMuebles {

    private String tipoMuebleSala;
    public MueblesSalon(){
        super();
    }


    public MueblesSalon(String nombreMueble, int medida, String calidad, String movilidad, LocalDate fechaCompra, String tipoMuebleSala){
        super(nombreMueble, medida, calidad, movilidad, fechaCompra);
        this.tipoMuebleSala=tipoMuebleSala;
    }

    public String getTipoMuebleSala() {
        return tipoMuebleSala;
    }

    public void setTipoMuebleSala(String tipoMuebleSala) {
        this.tipoMuebleSala = tipoMuebleSala;
    }

    @Override
    public String toString() {
        return "Datos del Mueble--> "+
                "\n Nombre Mueble: "+getNombreMueble()+
                "\n Medida: " + getMedida()+
                "\n Calidad: " + getCalidad()+
                "\n Movilidad: "+ getMovilidad()+
                "\n Fecha Compra: "+ getFechaCompra()+
                "\n Tipo Mueble: " + getTipoMuebleSala()

                ;
    }
}
