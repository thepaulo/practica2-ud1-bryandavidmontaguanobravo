package davidmontaguano.tiendamuebles.gui;

import davidmontaguano.tiendamuebles.base.MueblesRusticos;
import davidmontaguano.tiendamuebles.base.MueblesSalon;
import davidmontaguano.tiendamuebles.base.TiendaMuebles;
import davidmontaguano.tiendamuebles.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

public class MueblesControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private MueblesModelo modelo;
    private File uRutaExportada;


    public MueblesControlador(Ventana vista, MueblesModelo modelo){
        this.vista=vista;
        this.modelo=modelo;

        try{
            cargarDatosConfiguracion();

        }catch (IOException e){
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        String actionCommand= e.getActionCommand();

        switch(actionCommand){
            case "Nuevo":
                if(hayCamposVacios()){
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\nNombre Mueble" +
                            "\nMedida\nFecha"+ vista.salonRusticoLbl.getText());
                    break;
                }

                if(modelo.existeMueble(vista.nombreTxt.getText())){
                    Util.mensajeError("Ya existe un mueble con esa Nombre\n"+
                            vista.nombreTxt.getText());
                    break;
                }

                if(vista.salonRadioB.isSelected()){
                    modelo.altaSalon(vista.nombreTxt.getText(),Integer.parseInt(vista.medidaTxt.getText()),vista.calidadTxt.getText(),
                            vista.movilidadTxt.getText(),vista.fechaCompraDPicker.getDate(),vista.mSalonMaderaTxt.getText());
                }
                else{
                    modelo.altaRustico(vista.nombreTxt.getText(),Integer.parseInt(vista.medidaTxt.getText()),vista.calidadTxt.getText(),
                            vista.movilidadTxt.getText(),vista.fechaCompraDPicker.getDate(),vista.mSalonMaderaTxt.getText());
                }

                limpiarCampos();
                refrescar();
                break;

            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(uRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if(opt ==JFileChooser.APPROVE_OPTION){
                    try{
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    }catch (ParserConfigurationException ex){
                        ex.printStackTrace();
                    }catch (IOException ex){
                        ex.printStackTrace();
                    }catch (SAXException ex){
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(uRutaExportada,"Archivo XML", "xml");
                int opt2=selectorFichero2.showSaveDialog(null);
                if(opt2 == JFileChooser.APPROVE_OPTION){
                    try{
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    }catch (ParserConfigurationException ex){
                        ex.printStackTrace();
                    }catch (TransformerException ex){
                        ex.printStackTrace();
                    }
                }
                break;

            case "MueblesSalon":
                vista.salonRusticoLbl.setText("Muebles de Salon");
                break;

            case "MueblesRusticos":
                vista.salonRusticoLbl.setText("Muebles Rusticos");
                break;

        }
    }

    private boolean hayCamposVacios(){
     if (vista.mSalonMaderaTxt.getText().isEmpty() ||
         vista.nombreTxt.getText().isEmpty()||
         vista.medidaTxt.getText().isEmpty()||
         vista.calidadTxt.getText().isEmpty()||
         vista.movilidadTxt.getText().isEmpty()||
         vista.fechaCompraDPicker.getText().isEmpty()){
         return  true;
     }
     return false;
    }

    private void limpiarCampos(){
        vista.mSalonMaderaTxt.setText(null);
        vista.medidaTxt.setText(null);
        vista.calidadTxt.setText(null);
        vista.movilidadTxt.setText(null);
        vista.fechaCompraDPicker.setDate(null);
        vista.nombreTxt.requestFocus();

    }

    public void refrescar(){
        vista.dlmTiendaMuebles.clear();
        for(TiendaMuebles unMueble : modelo.obtenerMuebles()){
            vista.dlmTiendaMuebles.addElement(unMueble);
        }
    }

    private void addActionListener(ActionListener listener){
        vista.salonRadioB.addActionListener(listener);
        vista.rusticoRadioB.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.importarBtn.addActionListener(listener);
    }

    private void addWindowListener(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener){
        vista.list1.addListSelectionListener(listener);
    }

    private void cargarDatosConfiguracion() throws IOException{
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("muebles.conf"));
        uRutaExportada = new File(configuracion.getProperty("uRutaExportada"));
    }

    private void actualizarDatosConfiguracion(File uRutaExportada){
        this.uRutaExportada=uRutaExportada;
    }

    private void guardarConfiguracion() throws  IOException{
        Properties configuracion = new Properties();
        configuracion.setProperty("uRutaExportada", uRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("muebles.conf"), "Datos configuracion muebles");
    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if(resp == JOptionPane.OK_OPTION){
            try{
                guardarConfiguracion();
            }catch (IOException e1){
                e1.printStackTrace();
            }
                System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {


        if(e.getValueIsAdjusting()){
            TiendaMuebles muebleSeleccionado = (TiendaMuebles)vista.list1.getSelectedValue();
            vista.nombreTxt.setText(muebleSeleccionado.getNombreMueble());
            vista.medidaTxt.setText(String.valueOf(muebleSeleccionado.getMedida()));
            vista.calidadTxt.setText(muebleSeleccionado.getCalidad());
            vista.medidaTxt.setText(muebleSeleccionado.getCalidad());
            vista.fechaCompraDPicker.setDate(muebleSeleccionado.getFechaCompra());


            if(muebleSeleccionado instanceof MueblesSalon){
                vista.salonRadioB.doClick();
                vista.mSalonMaderaTxt.setText(((MueblesSalon) muebleSeleccionado).getTipoMuebleSala());
            }
            else{
                vista.rusticoRadioB.doClick();
                vista.mSalonMaderaTxt.setText(((MueblesRusticos)muebleSeleccionado).getTipoMadera());
            }
        }
    }
}
