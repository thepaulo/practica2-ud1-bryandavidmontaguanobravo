package davidmontaguano.tiendamuebles.gui;

import davidmontaguano.tiendamuebles.base.MueblesRusticos;
import davidmontaguano.tiendamuebles.base.MueblesSalon;
import davidmontaguano.tiendamuebles.base.TiendaMuebles;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.crypto.dsig.TransformException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class MueblesModelo {
    private ArrayList<TiendaMuebles> listaMuebles;

    public MueblesModelo(){
        listaMuebles= new ArrayList<TiendaMuebles>();
    }

    public ArrayList<TiendaMuebles> obtenerMuebles() {
        return listaMuebles;
    }

    public void altaSalon(String nombreMueble, int medida, String calidad, String movilidad, LocalDate fechaCompra,String tipoMuebleSala){
        MueblesSalon nuevoSalon=new MueblesSalon(nombreMueble,medida,calidad,movilidad,fechaCompra,tipoMuebleSala);
        listaMuebles.add(nuevoSalon);
    }

    public void altaRustico(String nombreMueble, int medida, String calidad, String movilidad, LocalDate fechaCompra,String tipoMadera){
        MueblesRusticos nuevoRustico=new MueblesRusticos(nombreMueble,medida,calidad,movilidad,fechaCompra,tipoMadera);
        listaMuebles.add(nuevoRustico);
    }

    public boolean existeMueble(String nombreMueble){
        for (TiendaMuebles unMueble:listaMuebles){
            if(unMueble.getNombreMueble().equals(nombreMueble)){
                return true;
            }
        }
        return false;
    }

    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory= DocumentBuilderFactory.newInstance();
        DocumentBuilder builder=factory.newDocumentBuilder();
        DOMImplementation dom= builder.getDOMImplementation();
        Document documento=dom.createDocument(null,"xml",null);

        Element raiz = documento.createElement("Muebles");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoMueble=null, nodoDatos=null;
        Text texto= null;

        for(TiendaMuebles unMueble: listaMuebles){
            if(unMueble instanceof MueblesSalon){
                nodoMueble=documento.createElement("MueblesSalon");
            }
            else{
                nodoMueble=documento.createElement("MueblesRusticos");
            }

            raiz.appendChild(nodoMueble);

            nodoDatos=documento.createElement("nombreMueble");
            nodoMueble.appendChild(nodoDatos);
            texto=documento.createTextNode(unMueble.getNombreMueble());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("medida");
            nodoMueble.appendChild(nodoDatos);
            texto=documento.createTextNode(String.valueOf((unMueble).getMedida()));
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("calidad");
            nodoMueble.appendChild(nodoDatos);
            texto=documento.createTextNode(unMueble.getCalidad());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("movilidad");
            nodoMueble.appendChild(nodoDatos);
            texto=documento.createTextNode(unMueble.getMovilidad());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("fechaCompra");
            nodoMueble.appendChild(nodoDatos);
            texto=documento.createTextNode(unMueble.getNombreMueble().toString());
            nodoDatos.appendChild(texto);

            if(unMueble instanceof MueblesSalon){
                nodoDatos=documento.createElement("tipoMuebleSala");
                nodoMueble.appendChild(nodoDatos);
                texto=documento.createTextNode(((MueblesSalon) unMueble).getTipoMuebleSala());
                nodoDatos.appendChild(texto);

            }
            else{
                nodoDatos=documento.createElement("tipoMadera");
                nodoMueble.appendChild(nodoDatos);
                texto=documento.createTextNode(((MueblesRusticos) unMueble).getTipoMadera());
                nodoDatos.appendChild(texto);
            }


        }

        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source,resultado);



    }


    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException{
        listaMuebles = new ArrayList<TiendaMuebles>();

        MueblesSalon nuevoMueblesSalon= null;
        MueblesRusticos nuevoMueblesRusticos=null;

        DocumentBuilderFactory factory= DocumentBuilderFactory.newInstance();
        DocumentBuilder builder=factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos= documento.getElementsByTagName("*");

        for(int i=0;i < listaElementos.getLength();i++){
            Element nodoMueble=(Element) listaElementos.item(i);

            if(nodoMueble.getTagName().equals("MueblesSalon")){
                nuevoMueblesSalon= new MueblesSalon();
                nuevoMueblesSalon.setNombreMueble(nodoMueble.getChildNodes().item(0).getTextContent());
                nuevoMueblesSalon.setMedida(Integer.parseInt(nodoMueble.getChildNodes().item(1).getTextContent()));
                nuevoMueblesSalon.setCalidad(nodoMueble.getChildNodes().item(2).getTextContent());
                nuevoMueblesSalon.setMovilidad(nodoMueble.getChildNodes().item(3).getTextContent());
                nuevoMueblesSalon.setFechaCompra(LocalDate.parse(nodoMueble.getChildNodes().item(4).getTextContent()));
                nuevoMueblesSalon.setTipoMuebleSala(nodoMueble.getChildNodes().item(5).getTextContent());

                listaMuebles.add(nuevoMueblesSalon);
            }
            else{
                if(nodoMueble.getTagName().equals("MueblesRusticos")){
                    nuevoMueblesRusticos= new MueblesRusticos();
                    nuevoMueblesRusticos.setNombreMueble(nodoMueble.getChildNodes().item(0).getTextContent());
                    nuevoMueblesRusticos.setMedida(Integer.parseInt(nodoMueble.getChildNodes().item(1).getTextContent()));
                    nuevoMueblesRusticos.setCalidad(nodoMueble.getChildNodes().item(2).getTextContent());
                    nuevoMueblesRusticos.setMovilidad(nodoMueble.getChildNodes().item(3).getTextContent());
                    nuevoMueblesRusticos.setFechaCompra(LocalDate.parse(nodoMueble.getChildNodes().item(4).getTextContent()));
                    nuevoMueblesRusticos.setTipoMadera(nodoMueble.getChildNodes().item(5).getTextContent());

                    listaMuebles.add(nuevoMueblesRusticos);

                }

            }
        }

    }








}
