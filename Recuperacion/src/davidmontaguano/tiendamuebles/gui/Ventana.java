package davidmontaguano.tiendamuebles.gui;

import com.github.lgooddatepicker.components.DatePicker;
import davidmontaguano.tiendamuebles.base.TiendaMuebles;

import javax.swing.*;

public class Ventana {
    private JPanel panel1;
    public JFrame frame;
    public JRadioButton salonRadioB;
    public JRadioButton rusticoRadioB;
    public JTextField nombreTxt;
    public JTextField medidaTxt;
    public JTextField calidadTxt;
    public JTextField movilidadTxt;
    public JLabel Nombre;
    public JLabel Medida;
    public JLabel Calidad;
    public JLabel Movilidad;
    public JLabel FechaCompra;
    public DatePicker fechaCompraDPicker;
    public JLabel salonRusticoLbl;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JList list1;
    public JTextField mSalonMaderaTxt;

    public DefaultListModel<TiendaMuebles> dlmTiendaMuebles;

    public Ventana(){
        frame = new JFrame("Tienda de Muebles");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();

    }
    public void initComponents(){
        dlmTiendaMuebles=new DefaultListModel<TiendaMuebles>();
        list1.setModel(dlmTiendaMuebles);
    }
}
