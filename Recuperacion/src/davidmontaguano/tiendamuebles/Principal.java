package davidmontaguano.tiendamuebles;

import davidmontaguano.tiendamuebles.gui.MueblesControlador;
import davidmontaguano.tiendamuebles.gui.MueblesModelo;
import davidmontaguano.tiendamuebles.gui.Ventana;

public class Principal {

    public static void main(String[] args){
        Ventana vista = new Ventana();
        MueblesModelo modelo= new MueblesModelo();
        MueblesControlador controlador= new MueblesControlador(vista,modelo);
    }


}
